# Mouse Net

This application provides the user the ability to control a remote PC.

## Build Dependencies

- Libraries / Frameworks
	- libxtst-dev
	- Hyper Game Engine 1.2.0 [Hyper Game Engine](https://gitlab.com/Elatronion/hyper-game-engine) (and all it's dependencies)
- Compilers
	- gcc
	- mingw-w64
- Build Automation Tools
	- make

## Build Instructions

As of now, the makefile is designed with Linux in mind.
But you may compile to both Linux and Windows.

Once all depenencies are installed, run the makefile with the following parameters:
```bash
TARGET_OS=<linux/windows>
TARGET_CPU=<x86_64/i686>
```

# TODO

- [x] Simulate Keyboard Input
- [x] Simulate Mouse Left Click
- [ ] Simulate Mouse Right Click
- [ ] Simulate Mouse Movement
- [ ] Stream Screen
- [ ] Stream Audio
